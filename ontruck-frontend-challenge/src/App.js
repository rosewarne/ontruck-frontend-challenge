import React, { Component } from 'react';
// Assets
import './stylesheets/main.css';
// Components
import Header from './components/Header';
import Search from './components/Search';
import VehiclesGrid from './components/VehiclesGrid';
//import WidgetVehicles from './components/WidgetVehicles';
class App extends Component {
  render() {
    return (
      <div className="wrapped">
        <Header />
        <Search />
        <VehiclesGrid />
        {/* <WidgetVehicles /> */}
      </div>
    );
  }
}

export default App;

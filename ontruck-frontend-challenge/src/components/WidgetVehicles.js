// Dependencies
import React, { Component } from 'react';

// Assets
import '../stylesheets/components/WidgetVehicles.css';
import iconFullTrailer from '../images/full-trailer.svg'
//
   const DataDrivers = 'http://localhost:3000/drivers';
// const DataVehicles = 'http://localhost:3000/vehicles';

class WidgetVehicles extends Component {
  constructor(props) {
    super(props);

    this.state = {
      drivers: [],
    };
  }

  componentDidMount() {
    fetch(DataDrivers)
      .then(response => response.json())
      .then(data => this.setState({ drivers: data}));
  }
  render() {
    const {drivers} = this.state;
    const totalDrivers = Object.keys(drivers).length;
    return (
      <div className="fe-widget-vehicles-grid">
        <div className="fe-widget-vehicles-grid__item">
        {/* BOX */}
          <div className="widget-vehicles-box">
            <div className="widget-vehicles-box__icon">
              <img src={iconFullTrailer} alt=""/>
            </div>
            <div className="widget-vehicles-box__n-drivers">
              {totalDrivers} Drivers
            </div>
            {drivers.map(driver =>
              <div key={driver.id} className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">{driver.name}</span>
                <a className="widget-vehicles-box__driver-data__email" href={"mailto:" + driver.email} title={"Send mail to " + driver.name}>{driver.email}</a>
              </div>
            )}  
          </div>
        </div>
      </div>
    );
  }
}

export default WidgetVehicles;

// Dependencies
import React, { Component } from 'react';

// Assets
import iconFullTrailer from '../images/full-trailer.svg'
import iconBoxVan from '../images/box-van.svg'
import iconRigidTruck from '../images/rigid-truck.svg'
import iconVan from '../images/van.svg'
import '../stylesheets/components/WidgetVehicles.css';


class VehiclesGrid extends Component {
  render() {
    return (
      <div className="fe-widget-vehicles-grid">
          <div className="fe-widget-vehicles-grid__item">
          {/* BOX Full Trailer */}
            <div className="widget-vehicles-box">
              <div className="widget-vehicles-box__icon">
                <img src={iconFullTrailer} alt=""/>
              </div>
              <div className="widget-vehicles-box__n-drivers">
                3 Drivers
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
            </div>
          </div>
          {/* BOX Ridgit Truck*/}
          <div className="fe-widget-vehicles-grid__item">
            <div className="widget-vehicles-box">
              <div className="widget-vehicles-box__icon">
                <img src={iconRigidTruck} alt=""/>
              </div>
              <div className="widget-vehicles-box__n-drivers">
                2 Drivers
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
            </div>
          </div>
          {/* BOX Box Van*/}
          <div className="fe-widget-vehicles-grid__item">
            <div className="widget-vehicles-box">
              <div className="widget-vehicles-box__icon">
                <img src={iconBoxVan} alt=""/>
              </div>
              <div className="widget-vehicles-box__n-drivers">
                4 Drivers
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
            </div>
          </div>
          {/* BOX Van*/}
          <div className="fe-widget-vehicles-grid__item">
            <div className="widget-vehicles-box">
              <div className="widget-vehicles-box__icon">
                <img src={iconVan} alt=""/>
              </div>
              <div className="widget-vehicles-box__n-drivers">
                1 Driver
              </div>
              <div className="widget-vehicles-box__driver-data">
                <span className="widget-vehicles-box__driver-data__name">Dashawn Watsica</span>
                <a className="widget-vehicles-box__driver-data__email" href="mailto:Abby.Wilderman@yahoo.com" title="Mail to {{driverName}}">Abby.Wilderman@yahoo.com</a>
              </div>
            </div>
          </div>
          {/* BOX Empty*/}
          <div className="fe-widget-vehicles-grid__item">
            <div className="widget-vehicles-box">
              <div className="widget-vehicles-box__icon">
                <img src={iconVan} alt=""/>
              </div>
              <div className="widget-vehicles-box__n-drivers widget-vehicles-box__n-drivers--no-drivers">
                No Drivers
              </div>
              
            </div>
          </div>
        </div>
    );
  }
}

export default VehiclesGrid;

// Dependencies
import React, { Component } from 'react';

// Assets
import '../stylesheets/components/Header.css';

class Header extends Component {
  render() {
    return (
      <header className="fe-header">
        <h1 className="header__title">Vehicles</h1>
      </header>
    );
  }
}

export default Header;
